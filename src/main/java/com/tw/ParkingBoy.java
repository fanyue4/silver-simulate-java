package com.tw;

public class ParkingBoy {
    private final ParkingLot parkingLot;
    private String lastErrorMessage;

    public ParkingBoy(ParkingLot parkingLot) {
        this.parkingLot = parkingLot;
    }

    public ParkingTicket park(Car car) {
        // TODO: Implement the method according to test
        // <-start-
        ParkingTicket ticket = new ParkingTicket();
        ParkingResult parkingResult = parkingLot.park(car);

        if (parkingResult.isSuccess() == true) {
            ticket = parkingResult.getTicket();
        } else if (parkingLot.getAvailableParkingPosition() == 0) {
            lastErrorMessage = parkingResult.getMessage();
            ticket = null;
        }

        return ticket;
        // ---end->
    }

    public Car fetch(ParkingTicket ticket) {
        // TODO: Implement the method according to test
        // <-start-
        Car car = new Car();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        ParkingTicket originTicket = parkingBoy.park(new Car());
        FetchingResult fetchingResult = parkingLot.fetch(ticket);

        if (fetchingResult.isSuccess() == true) {
            car = fetchingResult.getCar();
        } else if (ticket != originTicket) {
            car = null;
            lastErrorMessage = fetchingResult.getMessage();
        } else if (ticket == null) {
            car = null;
            lastErrorMessage = fetchingResult.getMessage();
        } else {
            lastErrorMessage = fetchingResult.getMessage();
        }

        return car;
        // ---end->
    }

    public String getLastErrorMessage() {
        return lastErrorMessage;
    }
}
